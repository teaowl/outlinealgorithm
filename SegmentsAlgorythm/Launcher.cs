﻿using SegmentsAlgorythm;
using System;
using System.Collections.Generic;

namespace OutlineAlgorithm
{
    internal class Launcher
    {
        private static void Main(string[] args)
        {
            List<Outline> outlines = new List<Outline>();
            NewOutlineProcessor processor = new NewOutlineProcessor();
            Console.WriteLine("--- Создание тестовой ситуации ---");
            Console.WriteLine("(!) Для проведения теста нужно 2 или более отрезков, среди которых хотя бы"
                + " 2 могут быть соединены в контур. Допустимая погрешность: <0.001");
            Console.WriteLine("(!) Для создания отрезка, введите последовательно 4 координаты через пробел в формате: X1, Y2, X2, Y2," +
                " для точек начала и конца соответственно");
            Console.WriteLine("(!) Числа с плавающей точкой разделяются запятой [,]");
            Console.WriteLine("(!) Для обработки введённых данных введите stop");
            while (true)
            {
                try
                {
                    Console.WriteLine("Введите координаты отрезка: ");
                    String data = Console.ReadLine();
                    if (data.Contains("stop")) break;
                    //парсинг и обработка ошибок должны быть синхронными, для своевременной реакции на неверный ввод
                    String[] strCoords = data.Split();
                    float x1 = float.Parse(strCoords[0]);
                    float y1 = float.Parse(strCoords[1]);
                    float x2 = float.Parse(strCoords[2]);
                    float y2 = float.Parse(strCoords[3]);
                    Segment segment;
                    if (x1<x2) segment = new Segment(x1, y1, x2, y2);
                    else segment = new Segment(x2, y2, x1, y1);
                    outlines = processor.processSegment(segment).GetAwaiter().GetResult();
                }
                catch (ArgumentNullException)
                {
                    Console.WriteLine("Ошибка! один из аргументов пуст!");
                }
                catch (FormatException)
                {
                    Console.WriteLine("Ошибка! похоже, один из аргументов не является числом!");
                }
                catch (OverflowException)
                {
                    Console.WriteLine("Ошибка! одно или несколько из введённых чисел слишком велики!");
                }
                catch (IndexOutOfRangeException)
                {
                    Console.WriteLine("Ошибка! недостаточно аргументов!");
                }
            }
            Console.WriteLine("---Результаты работы программы---");
            foreach (Outline outline in outlines) {
                Console.WriteLine(outline);
            }
            Console.ReadKey();
        }
    }
}