﻿using System.Drawing;

namespace SegmentsAlgorythm
{
    /// <summary>
    /// Отрезок, заданный двумя точками
    /// </summary>
    public struct Segment
    {
        public PointF start;
        public PointF end;

        public struct Vector
        {
            public float x;
            public float y;
        }

        /// <summary>
        /// Инициализация отрезка двумя точками
        /// </summary>
        /// <param name="start">Точка начала отрезка</param>
        /// <param name="end">Точка конца отрезка</param>
        public Segment(PointF start, PointF end)
        {
            this.start = start;
            this.end = end;
        }

        /// <summary>
        /// Инициализация отрезка координатами точек
        /// </summary>
        /// <param name="x1">Координата x для точки начала</param>
        /// <param name="y1">Координата y для точки начала</param>
        /// <param name="x2">Координата x для точки конца</param>
        /// <param name="y2">Координата y для точки конца</param>
        public Segment(float x1, float y1, float x2, float y2)
        {
            start = new PointF(x1, y1);
            end = new PointF(x2, y2);
        }

        public override string ToString()
        {
            return "["+start.X+"|"+start.Y+"]" + "--" + "[" + end.X + "|" + end.Y + "]";
        }
    }
}