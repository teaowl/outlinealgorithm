﻿using SegmentsAlgorythm;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace OutlineAlgorithm
{
    public class NewOutlineProcessor
    {
        float accuracy;
        private List<Outline> outlines = new List<Outline>();
        private List<SweepLineEvent> prioritySet = new List<SweepLineEvent>();
        private EventComparator eventComparator = new EventComparator();

        public NewOutlineProcessor(float accuracy = 0.001f) {
            this.accuracy = accuracy;
        }

        public async Task<List<Outline>> processSegment(Segment segment)
        {
            SweepLineEvent segmentStartEvent;
            segmentStartEvent.POI = segment.start;
            segmentStartEvent.parent = segment;
            segmentStartEvent.TYPE = SweepLineEvent.START;

            SweepLineEvent segmentEndEvent;
            segmentEndEvent.POI = segment.end;
            segmentEndEvent.parent = segment;
            segmentEndEvent.TYPE = SweepLineEvent.END;

            prioritySet.Add(segmentEndEvent);
            prioritySet.Add(segmentStartEvent);
            //перераспределяем события на числовой прямой
            //p.s.Встроенные алгоритмы отлично оптимизированы - почти отсортированный массив пересортируется очень быстро
            //p.p.s.как ни велик соблазн положить сортировку в отдельный поток, без синхронизации нельзя
            prioritySet.Sort(eventComparator);
            await FindOutline();
            Console.WriteLine("---Priority Set---");
            foreach (SweepLineEvent seg in prioritySet)
            {
                Console.WriteLine(seg);
            }
            Console.WriteLine("---Result Set---");
            foreach (Outline outline in outlines)
            {
                Console.WriteLine(outline);
            }
            return outlines;
        }

        private Task FindOutline()
        {
            return Task.Run(() =>
            {
                List<SweepLineEvent> activeSet = new List<SweepLineEvent>();
                //обыскивать активный набор
                for (int temp = 0; temp < prioritySet.Count; temp++)
                {
                    activeSet.Add(prioritySet[temp]);

                    //добавление события - хороший момент для анализа, как и удаление
                    Console.WriteLine("---Active Set---");
                    foreach (SweepLineEvent e in activeSet)
                    {
                        Console.WriteLine(e);
                    }
                    //Перебираем активный набор
                    //Так как отрезки отсортированы, никогда не будет так, что отрезки, с совпадающими точками будут стоять далеко друг от друга
                    //Если ближайшие три отрезка никак не связаны между собой, то их можно постепенно удалять
                    for (int i = 0; i < activeSet.Count; i++) {
                        if (i < activeSet.Count - 1)
                        {
                            //Console.WriteLine("Анализ " + activeSet[i] + " и " + activeSet[i + 1]);
                            if (i < activeSet.Count - 1 && Math.Abs(activeSet[i].POI.X - activeSet[i + 1].POI.X)<accuracy)
                            {
                                Console.WriteLine("Проверка по X пройдена!");
                                if (Math.Abs(activeSet[i].POI.Y - activeSet[i + 1].POI.Y) < accuracy)
                                {
                                    Console.WriteLine("Проверка по Y тоже");
                                    AddOutline(activeSet[i].parent, activeSet[i + 1].parent);
                                } else Console.WriteLine("А проверка по Y не очень");
                            }
                            else Console.WriteLine("Проверки по обоим координатам провалены");
                            if (i != activeSet.Count - 1 && activeSet.Count >= 4 && activeSet[i].TYPE == SweepLineEvent.END)
                            {
                                Console.WriteLine("Заметающая прямая больше не пересекает отрезок " + activeSet[i].parent);
                                activeSet.RemoveAll(e => e.parent.Equals(activeSet[i].parent));
                            }
                        } else Console.WriteLine("Не хватает элементов для анализа!");
                        }
                    }
            });
        }

        private void AddOutline(Segment segment, Segment foundedSegment)
        {
            //исследование в уже существующем контуре
            Console.WriteLine("---исследование в уже существующем контуре---");
            Outline first = searchInOtherOutlines(segment);
            Outline second = searchInOtherOutlines(foundedSegment);
            //если отрезки состоят в одном и том же непустом контуре, значит, ничего делать не нужно
            if (first != null && first == second)
            {
                Console.WriteLine("Контур уже существует");
                return;
            }
                //первый есть..
                if (first != null)
            {
                Console.WriteLine("отрезок " + segment + " уже есть в контурах..");
                //..а второго нет(наиболее распространённая ситуация)
                if (second == null)
                {
                    Console.WriteLine("а отрезка " + foundedSegment + " в контурах ещё нет, добавляем...");
                    first.Add(foundedSegment);
                    Console.WriteLine("итоговый контур: " + first);
                }
                else
                {
                    //..или и второй есть
                    Console.WriteLine("отрезок " + foundedSegment + " тоже..");
                    Console.WriteLine("оба есть в контурах! ");
                    Console.WriteLine("все проверки считаются проваленными...");
                }
            }
            //..или нет первого
            else
            {
                Console.WriteLine("отрезка " + segment + " нет в контурах..");
                //..но есть второй..
                if (second != null)
                {
                    Console.WriteLine("но " + foundedSegment + " есть, добавим первый к нему..");
                    second.Add(segment);
                    Console.WriteLine("итоговый контур " + second);
                }
                //..или обоих нет
                else
                {
                    Console.WriteLine("отрезка " + foundedSegment + " тоже");
                    Outline newOutline = new Outline(segment, foundedSegment);
                    outlines.Add(newOutline);
                    Console.WriteLine("так как совпадение всё же обнаружено, создаётся новый контур: " + newOutline);
                }
            }
        }

        private Outline searchInOtherOutlines(Segment segment)
        {
            Outline found = null;
            foreach (Outline outline in outlines)
            {
                if (outline.Contains(segment)) found = outline;
            }
            return found;
        }

        private class EventComparator : IComparer<SweepLineEvent>
        {
            public int Compare(SweepLineEvent first, SweepLineEvent second)
            {
                return first.POI.X.CompareTo(second.POI.X);
            }
        }

        private struct SweepLineEvent
        {
            public string TYPE;
            public const string START = "START";
            public const string END = "END";
            public PointF POI; //point of interest
            public Segment parent;

            public override string ToString()
            {
                return "["+TYPE + " | " + POI + " | Parent:" + parent+"]";
            }
        }
    }
}