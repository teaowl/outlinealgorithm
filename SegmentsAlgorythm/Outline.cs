﻿using System.Collections.Generic;

namespace SegmentsAlgorythm
{
    /// <summary>
    /// Контур, составленный из отрезков
    /// </summary>
    public class Outline: List<Segment>
    {
        public Outline(params Segment[] segments){
            foreach (Segment seg in segments) {
                Add(seg);
            }
        }

        public override string ToString()
        {
            string data = "";
            foreach (Segment segment in this) data += segment;
            return data;
        }
        //специфический Equals участвует в некоторых важных проверках алгоритма
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            bool isEqual = true;
            Outline anotherOutline = obj as Outline;
            foreach (Segment segment in anotherOutline)
            {
                if (!this.Contains(segment)) isEqual = false;
            }
            return isEqual;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}